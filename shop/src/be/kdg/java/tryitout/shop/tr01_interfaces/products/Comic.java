package be.kdg.java.tryitout.shop.tr01_interfaces.products;

import java.util.Objects;

/**
 * @author Christian Cambier
 * @version 1.0     09/2022
 */
public class Comic extends Book {
    /*
      * Instance data member(s)
     */
    private String artist;
    
    /*
      Constructor(s)
     */
    public Comic( String title, int price) {
        this(null, null, price, null, title, null);
    }  // ctor
    
    public Comic(String code, String description, double price) {
        this(code, description, price, null, null, null);
    }  // ctor
    
    public Comic(String code, String description, double price, String author, String title, String artist) {
        super(code, description, price, author, title);
        setArtist(artist);
    }  // ctor
    
    /*
      Getter(s)/Setter(s)
     */
    public String getArtist() {
        return artist;
    }
    
    public final void setArtist(String artist) {
        this.artist = artist;
    }
    
    /*
       Method(s)
     */
    @Override
    public String toString() {
        return String.format("%s artist:%s  ", super.toString(), artist);
    }  // toString()
    
    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        if (!super.equals(other)) {
            return false;
        }
        
        Comic comic = (Comic) other;
        
        return Objects.equals(artist, comic.artist);
    }
    
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (artist != null ? artist.hashCode() : 0);
        return result;
    }
}  // class