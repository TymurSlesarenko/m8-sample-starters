package be.kdg.java.tryitout.shop.tr01_interfaces.client;

import be.kdg.java.tryitout.shop.tr01_interfaces.products.*;

/**
 * @author Christian Cambier
 * @version 1.0     09/2022
 */
public class InterfacesApp {
    
    public static void main(String[] args) {
        System.out.println("tr01_interfaces");
        System.out.println("--------------- \n");
        
        Book animalFarm = new Book("BK001", "Allegorical novel", 10, "George Orwell", "Animal farm");
        Product effectiveJava = new Book("BK002", "Computing", 25, "Joshua Bloch", "Effective Java");
    } // main()
    
    
}  // class
